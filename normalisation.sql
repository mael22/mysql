SELECT * FROM animaux.animal;
INSERT INTO `Espece` (`id`, `nom_courant`, `nom_latin`, `description`)
VALUES
	(1,'Chien','Canis canis','Bestiole à quatre pattes qui aime les caresses et tire souvent la langue'),
	(2,'Chat','Felis silvestris','Bestiole à quatre pattes qui saute très haut et grimpe aux arbres'),
	(3,'Tortue','Testudo hermanni','Bestiole avec une carapace très dure'),
	(4,'Perroquet','Alipiopsitta xanthops','Jolianimalanimal oiseau parleur vert et jaune');