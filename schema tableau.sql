SELECT * FROM schemapharma.entreprise;
INSERT INTO entreprise
VALUES 
('81173941600014','bayer pharma','callac','Toffer'),
('81173941600512','pierre fabre','paris','mael'),
('81173941600845','sanofi','new york','poutine'),
('81173941600001','omega pharma','pordic','macron'),
('81173941600772','elr dev','ibiza','may'),
('81173941600961','cerp','londres','trump');

SELECT * FROM schemapharma.employé;
INSERT INTO employé
VALUES 
(81,'morice','1990-02-03','2008-01-02','2000','morice','rhumato','bayer pharma'),
(80,'billy','1985-11-21','2005-12-20','2100','morice','rhumato','pierre fabre'),
(21,'bob','1960-04-05','2006-06-24','1500','morice','cardio','sanofi'),
(54,'boby','1974-01-29','2007-10-09','2200','morice','cardio','omega pharma'),
(87,'sandra','1963-11-20','2006-05-06','1800','viviane','cardio','elr dev'),
(06,'philip','1987-12-12','2007-08-26','1900','viviane','dermato','cerp'),
(31,'manu','1993-11-09','2005-09-11','1950','morice','dermato','bayer pharma'),
(52,'jean','1980-05-18','2008-12-02','2200','morice','dermato','pierre fabre'),
(91,'jannine','1972-12-18','2006-06-21','2300','viviane','cancéro','sanofi'),
(20,'simone','1979-11-20','2008-03-02','2500','morice','cancéro','omega pharma'),
(41,'morganne','1988-08-16','2006-07-28','2600','viviane','cardio','elr dev'),
(02,'viviane','1992-09-11','2007-12-12','3000','morice','cardio' ,'cerp');

SELECT * FROM schemapharma.secteur;
INSERT INTO secteur 
VALUES
('rhumato'),
('cardio'),
('dermato'),
('cancéro'); 

SELECT * FROM schemapharma.medicament_arc;
INSERT INTO medicament_arc 
VALUES
('04','paracetam','mal de tete','acide salicylique'),
('03','dolip','rhume','acitrétine'),
('02','placébo','soigne tout','allantoine'),
('01','homéopat','soigne rien','almitrine');

SELECT * FROM schemapharma.medicament;
INSERT INTO medicament 
VALUES
('11','jakavi','hyper tension','argent'),
('12','januvia','grippe céphalées','azote'),
('13','maalox','grippe','arsenic'),
('14','mabelio','gastro','zolpidem'),
('15','abacavir','tendinite','zopiclone'),
('16','abasaglar','inflammation','valsartan'),
('17','zaditen','conjonctivite','verapamil'),
('18','zalasta','arthrite','macrogol');

SELECT * FROM schemapharma.principe_actif;
INSERT INTO principe_actif 
VALUES
('paracetam','acide salicylique'),
('dolip','acitrétine'),
('placébo','allantoine'),
('homéopat','almitrine'),
('jakavi','argent'),
('januvia','azote'),
('maalox','arsenic'),
('mabelio','zolpidem'),
('abacavir','zopiclone'),
('abasaglar','valsartan'),
('zaditen','verapamil'),
('zalasta','macrogol');

SELECT * FROM schemapharma.pathologie;
INSERT INTO pathologie 
VALUES
('libellé1','descr1'),
('libellé2','descr2'),
('libellé3','descr3'),
('libellé4','descr4'),
('libellé5','descr5'),
('libellé6','descr6'),
('libellé7','descr7'),
('libellé8','descr8'),
('libellé9','descr9'),
('libellé10','descr10');

SELECT entreprise.*, employé.* FROM entreprise
JOIN employé
    ON entreprise.nom = employé.nom;

select * from employé
where nom and matricule in (siret = '81173941600014');

select * from employé
where nom and matricule in (entreprise = 'bayer pharma');

select * from employé
where nom and matricule in (entreprise = 'bayer pharma','pierre fabre','sanofi');

select * from employé
where nom and matricule in (secteur = 'cardio');

select * from employé
where nom and matricule in (dateEmb > '1989-12-30' and dateEmb < '1993-01-01');

select * from pathologie
order by pathologie desc;

select * from entreprise 
where (lieu = 'paris');

-- select * from medicament or medicament_arc

-- select * from medicament
-- where principe actif;







