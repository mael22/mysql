CREATE TABLE Employee (
  id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  author VARCHAR(80) NOT NULL,
  title VARCHAR(80) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=INNODB;
